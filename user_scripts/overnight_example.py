from bluesky.plans import scan
from bluesky.plan_stubs import sleep
from bluesky.utils import (
    separate_devices,
    all_safe_rewind,
    Msg,
    ensure_generator,
    short_uid as _short_uid,
)
import time
from ophyd.status import Status
from bluesky import plan_stubs as bps

# Define the gas mix for a given gas selection in ml/min
gas_flows = {"Mix1":(10,5,1),"Mix2":(3,12,6), "Mix3":(2,5,10)}

def set_gas(mfc_tuple):
    yield from mv(gas_dosing.massflow_contr1, mfc_tuple[0],gas_dosing.massflow_contr2, mfc_tuple[1],gas_dosing.massflow_contr3, mfc_tuple[2])


def ramp_and_trigger(gas_sel, temp_setpoint, temp_ramp, dwell, interval=5, min_gas_time=0, num_exp=1):
    """
    This plan sets the gas MFC and temperature controllers, then:
    1. Waits for the gas change to take effect.
    2. Ramps the temperature to the setpoint, triggering a detector along the way.
    3. Holds the temperature for a dwell time while continuing data acquisition.
    """
    
    # Set the Gas
    print(f"Setting the gas to {gas_sel} -> {gas_flows[gas_sel]}")
    yield from set_gas(gas_flows[str(gas_sel)])

    # Set the ramp rate
    print(f"Setting the temperature ramp rate to {temp_ramp}C/min")
    yield from bps.mv(reactor_cell.temperature_reg.ramp, temp_ramp)

    # Set the temperature
    print(f"Starting to change the temperature to {temp_setpoint}")
    complete_status = yield from bps.abs_set(reactor_cell.temperature_reg, temp_setpoint)

    print(f"Starting the acquire loop while temperature is changing")
    temp = 0
    while abs(temp - temp_setpoint) > 10:
        yield from count([my_sim_cam, reactor_cell.temperature_sam], num_exp)
        print(f"Sleeping {interval} sec")
        yield from bps.sleep(interval)
        print(f"Done sleeping")
        temp = yield from bps.rd(reactor_cell.temperature_sam)
        print(f"Current temperature {temp}")

    # Wait for the gas stabilization
    print(f"Waiting for gas stabilization ({min_gas_time} sec)")
    yield from bps.sleep(min_gas_time)

    val = yield from bps.rd(reactor_cell.temperature_sam)
    print(f"Temperature Achieved: {val}C")

    # Dwell time: Hold temperature and continue acquisition
    print(f"Entering dwell period ({dwell} sec)")
    start_time = bps.time.time()
    while bps.time.time() - start_time < dwell:
        yield from count([my_sim_cam], num_exp)
        print(f"Sleeping {interval} sec")
        yield from bps.sleep(interval)
        print(f"Done sleeping")

    print(f"Dwell time elapsed")

