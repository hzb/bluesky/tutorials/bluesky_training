#!/bin/bash

# Display usage instructions
show_help() {
    echo "Usage: ./script.sh [sh | down]"
    echo "Start the container as root"
    echo "  sh: Optional command to enter container"
    echo "  stop: Stop and remove the container"
}

# Check for help option
if [[ "$1" == "-h" || "$1" == "--h" || "$1" == "-help" || "$1" == "--help" || "$1" == "help" || "$1" == "h" ]]; then
    show_help
    exit 0
fi

# Fix the host's X11 permissions
xhost +

# Define the container name
target_container="blueskytraining"

# Function to deploy container
deploy_container() {
    docker run --privileged -it --name blueskytraining \
    --env DISPLAY=${DISPLAY} \
    --network host \
    --env PYTHONPATH=/opt/bluesky/beamlinetools:$PYTHONPATH \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v ${HOME}/.bluesky/beamlinetools-1:/opt/bluesky/beamlinetools \
    -v ${HOME}/.bluesky/tutorials/:/opt/bluesky/tutorials \
    -v ${HOME}/.bluesky/data:/opt/bluesky/data \
    -v ${HOME}/.bluesky/user_scripts:/opt/bluesky/user_scripts \
    -v ${HOME}/.bluesky/history.sqlite:/opt/bluesky/ipython/profile_root/history.sqlite \
    -v ${HOME}/.bluesky/images:/home/epics/images \
    --device=/dev/dri:/dev/dri \
    registry.hzdr.de/hzb/bluesky/tutorials/bluesky_training:latest \
    sh -c "sudo chown -R jovyan:users /opt/bluesky && \
           cd /opt/bluesky && \
           export PYTHONPATH=/opt/bluesky/beamlinetools:$PYTHONPATH && \
           python3 -m pip install -e beamlinetools > /dev/null 2>&1 && \
           python3 -m pip install -e /opt/bluesky/tutorials/bluesky-tutorial-utils > /dev/null 2>&1 && \
           $1"
}


# Cleanup function
cleanup() {
    echo "Removing container $target_container and exiting..."
    docker container rm -f "$target_container"
    echo "Stopping context containers"
    ( cd ${HOME}/.bluesky; docker compose down )
    # Add further cleanup steps if necessary
    exit 1
}

# Trap Ctrl+C and call the cleanup function
trap cleanup EXIT

command_to_run="$1"


# Check command arguments and execute corresponding actions
case "$command_to_run" in
    "stop")
        echo "Stopping and removing bluesky"
        docker container rm -f "$target_container"
        echo "Stopping context containers"
        ( cd ${HOME}/.bluesky; docker compose down )
        ;;
    "sh")
        deploy_container sh
        ;;
    *)
        echo "Starting context containers"
        ( cd ${HOME}/.bluesky; docker compose up -d )
        # If no command specified, deploy the container
        deploy_container "jupyter lab --ip '0.0.0.0' --no-browser --NotebookApp.token='' --NotebookApp.password=''"
        ;;
esac
