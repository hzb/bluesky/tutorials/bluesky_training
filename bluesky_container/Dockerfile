# Bluesky Base Dockerfile

##### shared environment stage #################################################

# Use Ubuntu 20.04 as the base image.
FROM jupyter/minimal-notebook:latest

##### developer / build stage #####################################
USER root
# Install essential build tools and utilities.
RUN apt update -y && apt upgrade -y
RUN apt install -y --no-install-recommends \
    git \
    libgl1-mesa-dri \
    libgl1-mesa-dri \
    tzdata \
    re2c \
    rsync \
    ssh-client \
    nano \
    sudo \
    supervisor

# Install the required system packages
RUN apt install -y libgl-dev libxkbcommon-dev libegl-dev libfontconfig-dev libdbus-1-dev \
    libxcb-*-dev libxkbcommon-x11-dev    
RUN apt install -y libzbar-dev

# Create directory structure and set permissions for all of /opt
RUN chown -R jovyan:users /opt

# Configure sudo for jovyan
RUN echo "jovyan ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/jovyan

# Switch to jovyan for all remaining operations
USER jovyan

# Install the required python packages with increased timeout and retries
RUN pip install --upgrade pip
COPY bluesky_container/requirements.txt .
RUN pip install --default-timeout=100 --retries 3 -r requirements.txt

RUN mkdir -p /opt/bluesky
#suitcase-specfile
RUN cd /opt/bluesky && git clone --branch main https://codebase.helmholtz.cloud/hzb/bluesky/suitcase-specfile.git

# Install the packages downloaded in bluesky.
RUN cd /opt/bluesky/suitcase-specfile && pip install .

# Clone the 'shell-scripts-container' Git repository into the '/opt' directory.
RUN cd /opt && git clone https://codebase.helmholtz.cloud/hzb/bluesky/core/source/shell-scripts-for-container.git

# Set the PATH environment variable to include '/opt/shell-scripts-for-container/bin'.
ENV PATH="/opt/shell-scripts-for-container/bin:$PATH"

# Create a directory structure and clone 'profile_root' Git repository.
RUN mkdir -p /opt/bluesky/ipython && cd /opt/bluesky/ipython

# Copy the startup files to the container
COPY bluesky_container/startup/ /opt/bluesky/ipython/profile_root/startup

# Set the working directory to '/bluesky/'.
WORKDIR /opt/bluesky/

# Add ipython_config.py to the container.
COPY bluesky_container/ipython_config.py /opt/bluesky/ipython/profile_root/ipython_config.py

RUN mkdir -p /opt/bluesky/deploy
RUN mkdir -p /opt/bluesky/storage
