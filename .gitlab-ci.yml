stages:   # gitlab ci    
   # List of stages for jobs, and their order of execution
  - build_branch # creates tarball of branch image
  - branch_scan # Scan the tarball with trivy
  - check_branch_image # Check whether the image is properly built
  - build_develop  # builds when the merge request is made into development branch
  - test_latest # tests when the image with tag latest is pushed
  - build_tag  # builds when the tag is pushed
  - test_tag  # tests when the tag is pushed

build_branch:  # Creating tarball for the each branch in the repository
  stage: build_branch
  image: 
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir tar_images
    # specify auths for kaniko executor
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    # if the image does not exist and there are changes (excluding README.md), build it
    - /kaniko/executor --context $CI_PROJECT_DIR --no-push --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH --tarPath $CI_PROJECT_DIR/tar_images/$CI_COMMIT_BRANCH.tar --single-snapshot --compressed-caching=false --use-new-run --cleanup
  artifacts:
    paths:
      - tar_images
    when: on_success
    expire_in: 10 minutes
  rules:
    - if: '$CI_COMMIT_BRANCH == "develop" || $CI_COMMIT_BRANCH == "$CI_DEFAULT_BRANCH"'
      when: never
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop" || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "$CI_DEFAULT_BRANCH" || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "release"'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      when: never
    - exists:
        - $CI_COMMIT_BRANCH
    - changes:
        - README.md
      when: never
    - changes:
        - '*'
  tags:
    - docker
branch_scan: # Scan the tarball with trivy
  stage: branch_scan
  image:
    name: docker.io/aquasec/trivy:latest
    entrypoint: [""]
  script:
    - mkdir scan_result
    - cd tar_images
    - trivy image --timeout 15m --offline-scan --input $CI_COMMIT_BRANCH.tar -f json -o ../scan_result/$CI_COMMIT_BRANCH.json --severity CRITICAL
  artifacts:
    paths:
      - scan_result # scan results
    when: on_success
    expire_in: 10 minutes
  rules:
    - if: '$CI_COMMIT_BRANCH == "develop" || $CI_COMMIT_BRANCH == "$CI_DEFAULT_BRANCH"'
      when: never
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop" || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "$CI_DEFAULT_BRANCH" || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "release"'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      when: never
    - exists:
        - $CI_COMMIT_BRANCH
    - changes:
        - README.md
      when: never
    - changes:
        - '*'
  tags:
    - docker

check_branch_image: # check for vulnerabilities and validate with crane
  stage: check_branch_image
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  script:
    - cd tar_images
    - file_name=${CI_COMMIT_REF_NAME} # Assuming you want to use the branch name
    - vulnerabilities=$(awk -F '[:,]' '/"Vulnerabilities"/ {gsub("[[:blank:]]+", "", $2); print $2}' "../scan_result/${file_name}.json")
    - |
      if [ -n "$vulnerabilities" ]; then
        echo "There are security issues with the image ${file_name}.Dockerfile. Image is not pushed to registry!"
        echo "Vulnerabilities found in ${file_name}.Dockerfile are: $vulnerabilities" > docker_latest_image.txt
        crane validate --tarball "$file_name.tar"
        echo "false" > docker_latest_image.txt
      else
        echo "There are no security issues with the image ${file_name}.Dockerfile."
        crane validate --tarball "$file_name.tar"
        echo "true" > docker_latest_image.txt
      fi
  artifacts:
    paths: # save the results 
      - tar_images
    expire_in: 10 minutes
  rules:
    - if: '$CI_COMMIT_BRANCH == "develop" || $CI_COMMIT_BRANCH == "$CI_DEFAULT_BRANCH"'
      when: never
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop" || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "$CI_DEFAULT_BRANCH" || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "release"'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      when: never
    - exists:
        - $CI_COMMIT_BRANCH
    - changes:
        - README.md
      when: never
    - changes:
        - '*'
  tags:
    - docker

build_develop: # This build will run at the merge to develop
  stage: build_develop 
  image: 
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    # specify auths for kaniko executor
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    # build the image with tag latest if the merge request is made into main
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/bluesky_container/Dockerfile --destination $CI_REGISTRY_IMAGE:latest --single-snapshot --compressed-caching=false --use-new-run --cleanup
  rules:
    - if: $CI_COMMIT_BRANCH == "develop"
      when: always
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "$CI_DEFAULT_BRANCH" || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "release"'
      when: never
    - changes:
        - README.md
      when: never
  tags:
    - docker

test_latest: # This test will run only for tag latest
  stage: test_latest
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  script:
    # specify auths for crane executor
    - crane auth login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    # validate the image with tag latest
    - if ! crane validate --remote $CI_REGISTRY_IMAGE:latest; then exit 1; fi > test_latest.log
  artifacts:
    paths:
      - test_latest.log
    expire_in: 7 days
  rules:
    - if: $CI_COMMIT_BRANCH == "develop"
      when: always
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "$CI_DEFAULT_BRANCH" || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "release"'
      when: never
    - changes:
        - README.md
      when: never
  tags:
    - docker


build_tag: # This build will run only for tags
  stage: build_tag
  image: 
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  only: # build only if a new tag is made
    - tags
  script:
    # specify auths for kaniko executor  
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    # if the image does not exist, build it with the added tag
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/bluesky_container/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME --single-snapshot --compressed-caching=false --use-new-run --cleanup
test_tag: # This test will run only for tags
  stage: test_tag
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  only:
    - tags
  script:
    # specify auths for crane executor
    - crane auth login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    # validate the image with tag latest
    - if ! crane validate --remote $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME; then exit 1; fi > test_tag.log
  artifacts:
    paths:
      - test_tag.log
    expire_in: 7 days
  tags:
    - docker


