import warnings
import collections

from IPython import get_ipython
user_ns = get_ipython().user_ns



    
def get_labeled_devices(user_ns=None, maxdepth=8):
    ''' Returns dict of labels mapped to devices with that label

        Parameters
        ----------
        user_ns : dict, optional
            The namespace to search on
            Default is to grab the namespace of the ipython shell.

        maxdepth: int, optional
            max recursion depth

        Returns
        -------
            A dictionary of (name, ophydobject) tuple indexed by device label.

        Examples
        --------
        Read devices labeled as motors:
            objs = get_labeled_devices()
            my_motors = objs['motors']
    '''    
    # could be set but lists are more common for users
    obj_list = collections.defaultdict(list)

    if maxdepth <= 0:
        warnings.warn("Recursion limit exceeded. Results will be truncated.")
        return obj_list

    if user_ns is None:
        from IPython import get_ipython
        user_ns = get_ipython().user_ns

    for key, obj in user_ns.items():
        # ignore objects beginning with "_"
        # (mainly for ipython stored objs from command line
        # return of commands)
        # also check its a subclass of desired classes
        
        if not key.startswith("_"):
            if hasattr(obj, '_ophyd_labels_'):
                # don't inherit parent labels
                labels = obj._ophyd_labels_
                for label in labels:
                    obj_list[label].append((key, obj))

                if is_parent(obj):
                    # Get direct children (not grandchildren).
                    try:
                        children = {k: getattr(obj, k)
                                # obj might be a Signal (no read_attrs).
                                for k in getattr(obj, 'read_attrs', [])
                                if '.' not in k}
                        # Recurse over all children.
                        for c_key, v in get_labeled_devices(
                                user_ns=children,
                                maxdepth=maxdepth-1).items():
                            items = [('.'.join([key, ot[0]]), ot[1]) for ot in v]
                            obj_list[c_key].extend(items)
                    except Exception as e:
                        if obj.name == 'pgm': # is this needed?
                            obj_list['pgm'].append(obj)
                        else:
                            print(f'########################################')
                            print(f'problem with obj {obj.name}')
                            print(f"the following Exception was raised: {e} ")

    # Convert from defaultdict to normal dict before returning.
    label_obj_dict = {k: sorted(v) for k, v in obj_list.items()}
    return label_obj_dict


def is_parent(dev):
    # return whether a node is a parent
    # should not have component_names, or if yes, should be empty
    # read_attrs needed to check it's an instance and not class itself
    return (not isinstance(dev, type) and getattr(dev, 'component_names', []))


def color_generator():
    while True:
        yield "black"
        yield "red"
