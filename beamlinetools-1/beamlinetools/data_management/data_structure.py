import time
import os
import socket
from datetime import datetime

from event_model import RunRouter


# from beamlinetools.beamline_config.base import RE
# from beamlinetools.beamline_config.file_export import *
from beamlinetools.callbacks.file_exporter import CSVCallback
from beamlinetools.data_management.specfile_management import spec_factory, new_spec_file


class DataStructure:
    """ 
    This class is used to create the datastructure.

    The class should be initialized in a startup file like this:
    from beamlinetools.data_management.data_structure import DataStructure    
    mds = DataStructure()
    
    Then from the ipython session one can create a new experiment
    mds.new_experiment(<'experimental_group'>)
    
    For differen parts of the experiment a new folder with a new specfile
    can be created with:
    mds.newdata(<'experiment_description'>)
        year = str(datetime.now().year)
        commissioning_folder = year + "_commissioning"
        commissioning_abs_path = os.path.join(self._bluesky_data, commissioning_folder)
        if not os.path.exists(commissioning_abs_path):
            os.makedirs(commissioning_abs_path)
        dm = CSVCallback(file_path=self._bluesky_data)
        dm.change_user(commissioning_folder)
        print(f"commissioning folder {commissioning_folder}")
        RE.subscribe(dm.receiver)
    The class takes care also of the persistence of different spec-scan numbers.

    """    

    def __init__(self,RE):
        self.RE = RE
        self._exp_name = None # defined by newdata or read from RE.md
        self._set_number =  None# defined by new_experiment, newdata or read from RE.md
        self._bluesky_data = "/opt/bluesky/data"
        self._exp_name = None
        self.csv = None
        self._base_name = None
        
        self._commissioning_folder = str(datetime.now().year) + "_commissioning"
        self._commissioning_abs_path = os.path.join(self._bluesky_data, 
                                                    self._commissioning_folder)
        
        self._create_commissioning_folder()
        self._load_existing_keys()
        self._init_and_subscribe_to_csv_export()
        self._set_csv_export_folder()
        self._init_and_subscribe_spec_factory()
        self.print_info()
    
    def print_info(self):
        if self._exp_name is not None and self._base_name is not None:
            data_folder = os.path.join("~/.bluesky/", self._exp_name, self._base_name)
            msg = f"The data will be exported in \n{data_folder}"
        else:
            data_folder = os.path.join("~/.bluesky/", self._commissioning_folder)
            msg = f"The data will be exported in \n{data_folder}"
        print()
        print()
        print("##################################################################")
        print(msg)
        print("##################################################################")
        print()
        print()

    def _init_and_subscribe_to_csv_export(self):
        self.csv = CSVCallback(file_path=self._bluesky_data)
        self.RE.subscribe(self.csv.receiver)

    def _set_csv_export_folder(self):
        if self._base_name is None: # if no experiment is set, we use commissioning folder
            self.csv.change_user(self._commissioning_folder)
        else:
            self.csv.change_user(os.path.join(self._bluesky_data,
                                                  self._exp_name,
                                                  self._base_name))

    def _init_and_subscribe_spec_factory(self):
        if self._base_name is None: # if no experiment is set, we use commissioning folder
            year = time.strftime('%Y')
            spec_factory.file_prefix = 'myspot'+year
            spec_factory.directory = self._commissioning_abs_path
        else:
            spec_factory.file_prefix = self._base_name
            spec_factory.directory = os.path.join(self._bluesky_data,
                                                  self._exp_name,
                                                  self._base_name)

        rr = RunRouter([spec_factory])
        self.RE.subscribe(rr)
    
    def commissioning(self):
        # set up csv export folder
        self.csv.change_user(self._commissioning_folder)
        
        # setup specfile
        year = time.strftime('%Y')
        spec_factory.file_prefix = 'myspot'+year
        spec_factory.directory = self._commissioning_abs_path
        
    def _create_commissioning_folder(self):
        if not os.path.exists(self._commissioning_abs_path):
            os.makedirs(self._commissioning_abs_path)    
        
    def _load_existing_keys(self):
        # exp_name
        try:
            self._exp_name = self.RE.md["exp_name"]
            self._experiment_dir = os.path.join(self._bluesky_data, self._exp_name)
        except:
            self._exp_name = None
        
        # base_name
        try:
            self._base_name = self.RE.md["base_name"]
        except:
            self._base_name = None

        # set_number
        try:
            self._set_number = self.RE.md["set_number"]
        except:
            self._set_number = None

        # exported_data_dir
        try:
            self._newdata_dir = self.RE.md['exported_data_dir']
        except:
            self._newdata_dir = None


    def new_experiment(self, exp_name):
        self.RE.md["exp_name"] = time.strftime('%Y-%m-%d')+'_'+exp_name
        self._exp_name = exp_name
        self._experiment_dir = os.path.join(self._bluesky_data, time.strftime('%Y-%m-%d')+'_'+self._exp_name)
        # Check if the directory exists
        if not os.path.exists(self._experiment_dir):
            # If it doesn't exist, create it
            os.makedirs(self._experiment_dir)
        self._set_number = 0
        self.RE.md["set_number"] = self._set_number
        
    def newdata(self,base_name):
        
        # increase the set number, we are not allowed to overwrite a old files
        self._set_number += 1
        
        # update self variables
        Ymd = time.strftime('%Y-%m-%d')
        self._base_name = Ymd+'_set'+"{:03d}".format(self._set_number)+'_'+base_name   
        self._newdata_dir = os.path.join(self._experiment_dir,self._base_name)
        
        if not os.path.exists(self._newdata_dir):
            # If it doesn't exist, create it
            
            os.makedirs(self._newdata_dir)
            print(f'Created new folder {self._newdata_dir}')
            self.RE.md['scan_id']=0 
        else:
            raise(f'Existing folder detected {self._newdata_dir}. This should not happen, ask beamline scientis what to do')
        
        # change folder for csv export
        self.csv.change_user(self._newdata_dir) 
        # change specifle name
        spec_factory.file_prefix = self._base_name
        spec_factory.directory = self._newdata_dir

        # Update RE.md with new values
        self.RE.md["set_number"] = self._set_number
        self.RE.md['base_name']  = self._base_name
        self.RE.md['specfile'] = os.path.join(self._newdata_dir,
                                              self._base_name)
        self.RE.md['hostname'] = socket.gethostname() # not sure why/if needed
        self.RE.md['exported_data_dir'] = self._newdata_dir
        if 'scan_id' in self.RE.md.keys():
            print(f'Next scan is number: {self.RE.md["scan_id"]+1}')
        else:
            print(f'Next scan is number: {0}')
        
