from suitcase.specfile import Serializer

def spec_single_factory(name, start_doc):

    serializer = Serializer(spec_factory.directory,file_prefix='aquarius_'+str(start_doc['scan_id']))

    return [serializer], []

# Export everything to a single file
# https://github.com/NSLS-II-CHX/profile_collection/blob/15202d1019def2a0132f0bdb74f54cb5450e8117/startup/99-bluesky.py#L258-L299
 
def spec_factory(name, start_doc):

    serializer = Serializer(spec_factory.directory, file_prefix=spec_factory.file_prefix, flush=True)
    return [serializer], []


def new_spec_file(directory, name):
    """
    set new specfile name:
    - path is fixed at /home/xf11id/specfiles/
    - name= xyz .spec will be added automatically
    calling sequence: new_spec_file(name='xyz')
    """
    spec_factory.file_prefix = name
    spec_factory.file_directory = directory
