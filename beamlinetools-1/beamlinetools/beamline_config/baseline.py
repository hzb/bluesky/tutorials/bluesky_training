from .beamline import *

# here the devices that will be read before and after a scan
baseline = []

# here the devices to be read during each scan, 
# but not plotted unless explicitly set in the plan
silent_devices = []

