# in this file all the imports useful to the beamline
from beamlinetools.utils.script_load_helper import simple_load

SL = simple_load(user_script_location='/opt/bluesky/user_scripts/')
load_user_script = SL.load_script
