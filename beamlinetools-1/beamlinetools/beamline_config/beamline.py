# Workarounds/patches
from ophyd.signal import EpicsSignalBase
EpicsSignalBase.set_defaults(connection_timeout= 5, timeout=200)

from ophyd import EpicsMotor

# simulated devices
from ophyd.sim import det1, det2, det3, det4, motor1, motor2, motor, noisy_det   # two simulated detectors

from bluesky.run_engine import RunEngine

RE = RunEngine({})

from beamlinetools.magics.simplify_syntax import Simplify
from bluesky.magics import BlueskyMagics
from IPython import get_ipython

# standard magics
get_ipython().register_magics(BlueskyMagics)
# Plans magics - it will override some standard magics
simplify = Simplify(get_ipython())
simplify.autogenerate_magics('/opt/bluesky/beamlinetools/beamlinetools/beamline_config/plans.py')
run_plan = simplify.execute_magic