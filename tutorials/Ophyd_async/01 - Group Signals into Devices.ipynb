{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "regular-telling",
   "metadata": {},
   "source": [
    "# Group Signals into Devices\n",
    "\n",
    "In this tutorial we will group multiple Signals into a simple custom Device,\n",
    "which enables us to conveniently connect to them and read them in batch.\n",
    "\n",
    "## Set up for tutorial\n",
    "\n",
    "We'll start our IOCs connected to simulated hardware, some of which implement a [random walk](https://en.wikipedia.org/wiki/Random_walk) that we will use.\n",
    "\n",
    "The IOCs may already be running in the background. Run this command to verify\n",
    "that they are running: it should produce output with STARTING or RUNNING on each line.\n",
    "In the event of a problem, edit this command to replace `status` with `restart all` and run again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "guilty-highland",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "decay                            RUNNING   pid 7232, uptime 0:00:03\n",
      "mini_beamline                    RUNNING   pid 7233, uptime 0:00:03\n",
      "random_walk                      RUNNING   pid 7234, uptime 0:00:03\n",
      "random_walk_horiz                RUNNING   pid 7235, uptime 0:00:03\n",
      "random_walk_vert                 RUNNING   pid 7236, uptime 0:00:03\n",
      "simple                           RUNNING   pid 7237, uptime 0:00:03\n",
      "thermo_sim                       RUNNING   pid 7238, uptime 0:00:03\n",
      "trigger_with_pc                  RUNNING   pid 7239, uptime 0:00:03\n"
     ]
    }
   ],
   "source": [
    "!../supervisor/start_supervisor.sh status"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "95cec10f-4861-4099-bcad-06f15b835d02",
   "metadata": {},
   "source": [
    "## Define a custom device"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8bf08ecd-6a8c-45b5-bc4d-af66a6b963b6",
   "metadata": {},
   "source": [
    "Let's try to do the same as in the previous example but instead of Ophyd we will use Ophyd Async\n",
    "```\n",
    "# Device 1:\n",
    "random-walk:horiz:dt\n",
    "random-walk:horiz:x\n",
    "\n",
    "# Device 2:\n",
    "random-walk:vert:dt\n",
    "random-walk:vert:x\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6329b4bd-a1d4-4faa-8225-b2e4d86dd928",
   "metadata": {},
   "source": [
    "## Declarative"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "3ffc2cd4-34c0-4d48-9ea5-319e28d85914",
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import Annotated as A\n",
    "from ophyd_async.core import (\n",
    "    SignalR,\n",
    "    SignalRW,\n",
    "    StandardReadable,\n",
    ")\n",
    "from ophyd_async.epics.core import EpicsDevice, PvSuffix\n",
    "from ophyd_async.core import StandardReadableFormat as Format\n",
    "\n",
    "\n",
    "class RandomWalk(EpicsDevice, StandardReadable):\n",
    "    x: A[SignalR[float], PvSuffix('x')]\n",
    "    dt: A[SignalRW[float], PvSuffix('dt')]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "452d7ea9-bfb1-4283-8506-2efae8e1075a",
   "metadata": {},
   "source": [
    "When the Device is instantiated, the EpicsDevice baseclass will look at all the type hints for annotations with a PvSuffix. PvSuffix also allows you to specify different suffixes for the read and write PVs if they are different.\n",
    "\n",
    "Now, we create instances of the device, providing the PV prefix that\n",
    "identifies one of our IOCs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "744769d1-9969-47b3-ae90-0eafef660656",
   "metadata": {},
   "outputs": [],
   "source": [
    "random_walk_horiz_async = RandomWalk(\"random-walk:horiz:\", name=\"random_walk_horiz_async\")\n",
    "random_walk_vert_async = RandomWalk(\"random-walk:vert:\", name=\"random_walk_vert_async\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f7bded5f-c38c-415b-9a9e-431fd3c5cfe7",
   "metadata": {},
   "source": [
    "## Use it with the Bluesky RunEngine\n",
    "\n",
    "The signals can be used by the Bluesky RunEngine. Let's configure a RunEngine\n",
    "to print a table.\n",
    "Rather than calling `Device.connect` yourself which would use the wrong event loop you can \n",
    "use `init_devices` at startup or the equivalent `ensure_connected`. Remember to \n",
    "pass `mock=True` during testing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "c6c41327-0ad2-4a44-ba6a-ae57691ffc50",
   "metadata": {},
   "outputs": [],
   "source": [
    "from bluesky import RunEngine\n",
    "RE = RunEngine({})\n",
    "\n",
    "from bluesky.plans import count\n",
    "from bluesky.callbacks import LiveTable\n",
    "token = RE.subscribe(LiveTable([\"random_walk_horiz_async-x\", \"random_walk_horiz_async-dt\"]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "3b69f0e6-defb-4fa0-b287-2e2bb1abac60",
   "metadata": {},
   "outputs": [],
   "source": [
    "from ophyd_async.core import init_devices\n",
    "with init_devices():\n",
    "    random_walk_horiz_async = RandomWalk(\"random-walk:horiz:\", name=\"random_walk_horiz_async\")\n",
    "    random_walk_vert_async = RandomWalk(\"random-walk:vert:\", name=\"random_walk_vert_async\")\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "5a6eff0c-1aab-46ab-94ce-ac18edd8995d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n",
      "+-----------+------------+---------------------------+\n",
      "|   seq_num |       time | random_walk_horiz_async-x |\n",
      "+-----------+------------+---------------------------+\n",
      "|         1 | 18:51:54.6 |                        17 |\n",
      "|         2 | 18:51:55.6 |                        17 |\n",
      "|         3 | 18:51:56.6 |                        17 |\n",
      "+-----------+------------+---------------------------+\n",
      "generator count ['02e44633'] (scan num: 1)\n",
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "('02e44633-11e5-46b4-9161-e0546595e4f0',)"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "RE(count([random_walk_horiz_async.x], num=3, delay=1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "22383a00-cf70-4924-bbf6-55a41766b6c6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n",
      "+-----------+------------+---------------------------+----------------------------+\n",
      "|   seq_num |       time | random_walk_horiz_async-x | random_walk_horiz_async-dt |\n",
      "+-----------+------------+---------------------------+----------------------------+\n",
      "|         1 | 18:52:10.0 |                        17 |                          3 |\n",
      "|         2 | 18:52:11.0 |                        17 |                          3 |\n",
      "|         3 | 18:52:12.0 |                        16 |                          3 |\n",
      "+-----------+------------+---------------------------+----------------------------+\n",
      "generator count ['816fa8f1'] (scan num: 2)\n",
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "('816fa8f1-0fc4-4f4f-b06d-5f0277ca2135',)"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "RE(count([random_walk_horiz_async.x, random_walk_horiz_async.dt], num=3, delay=1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "5d79416a-ea66-4a14-9b5d-1c552a41fbc6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n",
      "+-----------+------------+---------------------------+----------------------------+\n",
      "|   seq_num |       time | random_walk_horiz_async-x | random_walk_horiz_async-dt |\n",
      "+-----------+------------+---------------------------+----------------------------+\n",
      "|         1 | 18:55:43.2 |                           |                            |\n",
      "|         2 | 18:55:44.2 |                           |                            |\n",
      "|         3 | 18:55:45.2 |                           |                            |\n",
      "+-----------+------------+---------------------------+----------------------------+\n",
      "generator count ['46f6ce04'] (scan num: 3)\n",
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "('46f6ce04-e555-4d11-98d9-88a806871f7d',)"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "RE(count([random_walk_horiz_async], num=3, delay=1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "comparable-boost",
   "metadata": {},
   "source": [
    "## Assign a \"Kind\" to Components\n",
    "\n",
    "In the example just above, notice that we are recording either `random_walk_horiz_async-x` or `random_walk_horiz_async-dt`. But if we want to read both signals we don't get any readbacks. To address this issue we need to assign Kind to the components. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "8c08784a-b26e-42b9-af18-1e194d6af720",
   "metadata": {},
   "outputs": [],
   "source": [
    "class RandomWalk(EpicsDevice, StandardReadable):\n",
    "    x: A[SignalR[float], PvSuffix('x'), Format.CONFIG_SIGNAL]\n",
    "    dt: A[SignalRW[float], PvSuffix('dt'), Format.HINTED_SIGNAL]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "8ba921dd-b0a9-4530-9d63-cd6d9252b592",
   "metadata": {},
   "outputs": [],
   "source": [
    "from ophyd_async.core import init_devices\n",
    "with init_devices():\n",
    "    random_walk_horiz_async = RandomWalk(\"random-walk:horiz:\", name=\"random_walk_horiz_async\")\n",
    "    random_walk_vert_async = RandomWalk(\"random-walk:vert:\", name=\"random_walk_vert_async\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a3b1b8d-0f2b-44d3-8870-b657a4566d27",
   "metadata": {},
   "source": [
    "The result is that ``random_walk_horiz-x`` is moved from ``read()`` to\n",
    "``read_configuration()``:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "605e1cfd-94ba-4e48-a983-ee2e646e6801",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'random_walk_horiz_async-x': {'value': -6.457687581149361,\n",
       "  'timestamp': 1740421158.948642,\n",
       "  'alarm_severity': 0}}"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "await random_walk_horiz_async.read_configuration()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "id": "b67facaf-7776-4ce8-a3b0-4587b334d282",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'random_walk_horiz_async-dt': {'value': 3.0,\n",
       "  'timestamp': 1740403362.187948,\n",
       "  'alarm_severity': 0}}"
      ]
     },
     "execution_count": 34,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "await random_walk_horiz_async.read()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "7550c649-51cd-4782-8ecf-e96ad28b6ae7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\n",
      "+-----------+------------+---------------------------+----------------------------+\n",
      "|   seq_num |       time | random_walk_horiz_async-x | random_walk_horiz_async-dt |\n",
      "+-----------+------------+---------------------------+----------------------------+\n",
      "|         1 | 19:19:21.6 |                           |                          3 |\n",
      "|         2 | 19:19:22.6 |                           |                          3 |\n",
      "|         3 | 19:19:23.6 |                           |                          3 |\n",
      "+-----------+------------+---------------------------+----------------------------+\n",
      "generator count ['5c7d825d'] (scan num: 5)\n",
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "('5c7d825d-1b00-4dfe-89ab-f9683d345eb3',)"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "RE(count([random_walk_horiz_async], num=3, delay=1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "white-kruger",
   "metadata": {},
   "source": [
    "In Bluesky's Document Model, the result of ``device.read()`` is placed in an\n",
    "Event Document, and the result of ``device.read_configuration()`` is placed in\n",
    "an Event Descriptor document. The Bluesky RunEngine always calls\n",
    "``device.read_configuration()`` and captures that information the first time\n",
    "a given ``device`` is read.\n",
    "\n",
    "Here’s a concise list of the signal types:  \n",
    "\n",
    "- **CONFIG_SIGNAL** – Used in `read_configuration()` and `describe_configuration()`.  \n",
    "- **HINTED_SIGNAL** – Used in `read()` and `describe()`, also adds to `hints`.  \n",
    "- **UNCACHED_SIGNAL** – Used in `read()` and `describe()`, but not cached.  \n",
    "- **HINTED_UNCACHED_SIGNAL** – Like `UNCACHED_SIGNAL`, but also adds to `hints`.  \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "id": "e8a4a0bf-aed9-492f-8880-0d0aeff14e78",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1"
      ]
     },
     "execution_count": 32,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def print_docs_to_stdout(name, doc):\n",
    "    print(f\"----------- {name} --------------\")\n",
    "    print(f\"{doc = }\")\n",
    "    print(f\"-------------------------\\n\")\n",
    "\n",
    "RE.subscribe(print_docs_to_stdout)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "id": "029bcb81-713c-4765-a733-d531a6debde2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "----------- start --------------\n",
      "doc = {'uid': '97fcfb2e-4ce8-4c14-a1ac-c7a9561624b1', 'time': 1740421594.177668, 'versions': {'ophyd': '1.10.0', 'bluesky': '1.13.1rc3.dev4+gea61fb99'}, 'scan_id': 6, 'plan_type': 'generator', 'plan_name': 'count', 'detectors': ['random_walk_horiz_async'], 'num_points': 3, 'num_intervals': 2, 'plan_args': {'detectors': ['<__main__.RandomWalk object at 0x7f6bb353b0d0>'], 'num': 3, 'delay': 1}, 'hints': {'dimensions': [(('time',), 'primary')]}}\n",
      "-------------------------\n",
      "\n",
      "\n",
      "\n",
      "+-----------+------------+---------------------------+----------------------------+\n",
      "|   seq_num |       time | random_walk_horiz_async-x | random_walk_horiz_async-dt |\n",
      "+-----------+------------+---------------------------+----------------------------+\n",
      "----------- descriptor --------------\n",
      "doc = {'configuration': {'random_walk_horiz_async': {'data': {'random_walk_horiz_async-x': 9.13604359601222}, 'timestamps': {'random_walk_horiz_async-x': 1740421592.068643}, 'data_keys': {'random_walk_horiz_async-x': {'dtype': 'number', 'shape': [], 'dtype_numpy': '<f8', 'source': 'ca://random-walk:horiz:x', 'units': '', 'precision': 0}}}}, 'data_keys': {'random_walk_horiz_async-dt': {'dtype': 'number', 'shape': [], 'dtype_numpy': '<f8', 'source': 'ca://random-walk:horiz:dt', 'units': '', 'precision': 0, 'object_name': 'random_walk_horiz_async'}}, 'name': 'primary', 'object_keys': {'random_walk_horiz_async': ['random_walk_horiz_async-dt']}, 'run_start': '97fcfb2e-4ce8-4c14-a1ac-c7a9561624b1', 'time': 1740421594.2248664, 'uid': '53f5d3ed-63dc-4664-99fa-b1f898585f17', 'hints': {'random_walk_horiz_async': {'fields': ['random_walk_horiz_async-dt']}}}\n",
      "-------------------------\n",
      "\n",
      "|         1 | 19:26:34.2 |                           |                          3 |\n",
      "----------- event --------------\n",
      "doc = {'uid': 'b7f00273-29e3-4513-9617-83475095f6ac', 'time': 1740421594.233822, 'data': {'random_walk_horiz_async-dt': 3.0}, 'timestamps': {'random_walk_horiz_async-dt': 1740403362.187948}, 'seq_num': 1, 'filled': {}, 'descriptor': '53f5d3ed-63dc-4664-99fa-b1f898585f17'}\n",
      "-------------------------\n",
      "\n",
      "|         2 | 19:26:35.1 |                           |                          3 |\n",
      "----------- event --------------\n",
      "doc = {'uid': '3ae62218-f55f-417e-95d2-181c82718023', 'time': 1740421595.184556, 'data': {'random_walk_horiz_async-dt': 3.0}, 'timestamps': {'random_walk_horiz_async-dt': 1740403362.187948}, 'seq_num': 2, 'filled': {}, 'descriptor': '53f5d3ed-63dc-4664-99fa-b1f898585f17'}\n",
      "-------------------------\n",
      "\n",
      "|         3 | 19:26:36.1 |                           |                          3 |\n",
      "----------- event --------------\n",
      "doc = {'uid': '4fa1cc54-a9b3-4c04-b71f-104e3564b642', 'time': 1740421596.1864538, 'data': {'random_walk_horiz_async-dt': 3.0}, 'timestamps': {'random_walk_horiz_async-dt': 1740403362.187948}, 'seq_num': 3, 'filled': {}, 'descriptor': '53f5d3ed-63dc-4664-99fa-b1f898585f17'}\n",
      "-------------------------\n",
      "\n",
      "+-----------+------------+---------------------------+----------------------------+\n",
      "generator count ['97fcfb2e'] (scan num: 6)\n",
      "\n",
      "\n",
      "----------- stop --------------\n",
      "doc = {'uid': '4bd4d7e4-1391-40d9-9845-73fbf4466687', 'time': 1740421597.186969, 'run_start': '97fcfb2e-4ce8-4c14-a1ac-c7a9561624b1', 'exit_status': 'success', 'reason': '', 'num_events': {'primary': 3}}\n",
      "-------------------------\n",
      "\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "('97fcfb2e-4ce8-4c14-a1ac-c7a9561624b1',)"
      ]
     },
     "execution_count": 33,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "RE(count([random_walk_horiz_async], num=3, delay=1))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
